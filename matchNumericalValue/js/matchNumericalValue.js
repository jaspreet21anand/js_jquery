function form(element) {
  this.element = element;
}

form.prototype.validateNumber = function() {
  if(Number(this.element.children[0].value)) {
    this.element.children[2].value = "True"
    event.preventDefault();
    var _this = this;
    setTimeout(function() { _this.element.submit(); }, 2000);
  } else {
    event.preventDefault();
    this.element.children[2].value = "False"
  }
};

form.prototype.bindEvents = function() {
  var _this = this;
  this.element.addEventListener("submit", function() { _this.validateNumber() });
};

window.onload = function() {
  form1 = new form(document.getElementById('form1'));
  form1.bindEvents();
}