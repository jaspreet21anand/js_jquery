function FetchJSON() {
  this.cachedDaySpecials = {};
  this.$formElement = $("#specials > form");
};

FetchJSON.prototype.createDiv = function() {
  this.$div = $("<div />", { 'id':'daySpecials' });
};

FetchJSON.prototype.sendAJAXAndStoreJSON = function(selectedValue) {
  var _this = this;
  $.get( "js/specials.json", function(daySpecialsJSON) {
    _this.cachedDaySpecials = daySpecialsJSON;
    _this.displayValues(selectedValue);
  }, "json");
};

FetchJSON.prototype.getSpecialDayValues = function() {
  var selectedValue = this.$formElement.find(":selected").val();
  this.$div.empty(); //empty the div
  if(this.cachedDaySpecials[selectedValue]) {
      this.displayValues(selectedValue);
  } else if(selectedValue.trim()) {
      this.sendAJAXAndStoreJSON(selectedValue);
  }

};

FetchJSON.prototype.displayValues = function(selectedValue) {
   var title = $("<h2>").text(this.cachedDaySpecials[selectedValue]["title"]),
       image = $("<img>").attr("src", this.cachedDaySpecials[selectedValue]["image"]);
       text = $("<p>").text(this.cachedDaySpecials[selectedValue]["text"]).css("color",
              this.cachedDaySpecials[selectedValue]["color"]);
   this.$div.append(title, image, text); //append in div
   this.$formElement.after(this.$div);
};

FetchJSON.prototype.removeSubmitButton = function() {
  this.$formElement.find("li:last").remove();
};

FetchJSON.prototype.bindEvents = function(selectList) {
  var _this = this;
  this.$formElement.find("select").on("change", function() {
    _this.getSpecialDayValues();
  });
};

FetchJSON.prototype.init = function() {
  this.createDiv()
  this.bindEvents();
  this.removeSubmitButton();
};

$(document).ready(function() {
  var json = new FetchJSON();
  json.init();
});