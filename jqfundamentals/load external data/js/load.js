 /**
  *args = {
  *  "sourceUrl" : "url of the source page",
  *  "targetElement" : "the container element of the text of data"
  *}
  */
function LoadData(args) {
  var containingElement = "div",
      containingElementClass = "module",
      titleTags = "h3";
  this.$headings = $(containingElement + "." + containingElementClass + " " + titleTags);

  //a space character is concatenated after url as needed by the $destination.load() method to separate selector
  this.dataUrl = args["sourceUrl"] + " ";
  this.externalDataContainingElements = args["targetElement"];
};

LoadData.prototype.addElementsUnderHeading = function() {
  this.$headings.after("<" + this.externalDataContainingElements + ">");
};

LoadData.prototype.connectDataUrlAndIdToHeading = function() {
  var _this = this;
  this.$headings.each(function() {
    var $heading = $(this),
        dataId = $heading.find(":first-child").attr("href").substr(9);
    $heading.next(this.externalDataContainingElements).data("Url-Id", _this.dataUrl + dataId);
  });
};


LoadData.prototype.loadDataInElement = function($targetElement) {
  $targetElement.load($targetElement.data("Url-Id"));
};

LoadData.prototype.bindEvents = function() {
  var _this = this;
  this.$headings.click(function(event) {
    event.preventDefault();
    var $targetElement = $(this).next(_this.externalDataContainingElements);
    if(!$targetElement.children().length) {
      _this.loadDataInElement($targetElement);
    } else {
        $targetElement.empty();
    }
  });
};

LoadData.prototype.init = function() {
  this.addElementsUnderHeading();
  this.connectDataUrlAndIdToHeading();
  this.bindEvents();
};

$(function() {
  (new LoadData({
    "sourceUrl" : "./data/Blog.html ", 
    "targetElement" : "div"
  })).init();
});