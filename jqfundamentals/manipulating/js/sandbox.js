//EXERCISE - 3
//append five elements
$("#myList").append($("<li><li><li><li><li>"))

//remove odd elements
//even takes odd numbers of elements
$("#myList li:even").remove()

//add h2 and p in last div
$("div.module").last().append($("<h2><p>"))

//append option in select element containing value as Wednesday
$("select[name='day']").append("<option>Wednesday</option>")

//add div after last div, give it class module, add a existing image to it
$("div:last").after($("<div>")).next().addClass("module").append($("img:first").clone())
