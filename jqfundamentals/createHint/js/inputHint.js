function Hint(element) {
  this.element = element;
  this.hintObject = {
    hint : '',
    isValueEnteredInTextBox : false
  };
};

Hint.prototype.createHint = function(first_argument) {
  var label = this.fetchLabel();
  this.hintObject.hint = label.text();
  label.remove();
};


Hint.prototype.fetchLabel = function() {
  return $("label[for=" + this.element.attr("id") + "]");
};

Hint.prototype.bindEvents = function() {
  var _this = this;
  _this.element.focus(function() { _this.removeHint() });
  _this.element.blur(function() { _this.showHint() });
};

Hint.prototype.removeHint = function() {
  if(!this.hintObject.isValueEnteredInTextBox) {
    this.element.val('');
    this.element.removeClass("hint");
  }
};

Hint.prototype.showHint = function() {
  var value = this.element.val().trim();
  if(!value) {
    this.element.val(this.hintObject.hint);
    this.element.addClass("hint");
    this.hintObject.isValueEnteredInTextBox = false;
  } else {
    this.hintObject.isValueEnteredInTextBox = true;
  }
};

Hint.prototype.init = function() {
  this.createHint();
  this.bindEvents();
  this.showHint();
};

$(document).ready(function() {
  var hintObject = new Hint($("#q"));
  hintObject.init();
});
