function DropDownOnHover() {
  this.elements = $("#nav > li")
};

DropDownOnHover.prototype.addClass = function(li) {
  $(li).children("ul").addClass("hover");
};

DropDownOnHover.prototype.removeClass = function(li) {
  $(li).children("ul").removeClass("hover");
};

DropDownOnHover.prototype.bindEvents = function() {
  var _this = this;
  _this.elements.hover(function() {
    _this.addClass(this);
  }, function() {
      _this.removeClass(this);
    });
};

$(document).ready(function() {
  var dropDown = new DropDownOnHover();
  dropDown.bindEvents();
});