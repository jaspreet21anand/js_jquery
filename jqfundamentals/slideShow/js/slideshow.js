function SlideShow(tabNavigator) {
  this.$photoElement =  null;
  this.tabNavigator = tabNavigator;
};

SlideShow.prototype.fadeOutIn = function($nextPhotoElement) {
  var _this = this;
  _this.toggleClass($nextPhotoElement);
  this.$photoElement.data("divId").fadeOut(function() {
      $nextPhotoElement.data("divId").fadeIn();
  });
  this.$photoElement = $nextPhotoElement;
}

SlideShow.prototype.startSlideShow = function() {
  var _this = this;
  _this.initialPhotoShow();
  setInterval(function() {
    if(_this.$photoElement[0] == _this.tabNavigator.$listItems.last()[0]) {
      _this.fadeOutIn(_this.tabNavigator.$listItems.first());
      _this.$photoElement = _this.tabNavigator.$listItems.first();
    } else {
        _this.fadeOutIn(_this.$photoElement.next('li'))
    }
  }, 2000);
};

SlideShow.prototype.initialPhotoShow = function() {
  this.$photoElement = this.tabNavigator.$listItems.first();
  this.tabNavigator.addCurrentClass(this.$photoElement);
  this.$photoElement.data("divId").fadeIn()
};

SlideShow.prototype.toggleClass = function($nextPhotoElement) {
  this.tabNavigator.removeCurrentClass();
  this.tabNavigator.addCurrentClass($nextPhotoElement);
};

SlideShow.prototype.moveItemsToTop = function(id) {
  var element = $("#" + id).detach();
  $("body").prepend(element);
};

$(document).ready(function() {
  var tabNavigator = new TabbedNavigator("slideshow")
  var slideShow = new SlideShow(tabNavigator);
  tabNavigator.init(slideshow)
  slideShow.moveItemsToTop("slideshow");
  slideShow.startSlideShow();
});