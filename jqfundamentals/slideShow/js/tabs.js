function TabbedNavigator (id) {
  this.$elementsToDisplay = $("#" + id).children('li');
  this.$headings = this.$elementsToDisplay.find("h2");
}

TabbedNavigator.prototype.hideVisibleElements = function() {
  this.$elementsToDisplay.filter(":visible").hide();
};

TabbedNavigator.prototype.appendULToDOM = function() {
  var _this = this;
  _this.$ul = $("<ul />", { 'class':'tabNavigator' });
  _this.$elementsToDisplay.each(function() {
    var $this = $(this);
    _this.$ul.append($("<li />", { 'text':($this.find("h2")).text(), 'data':{'divId':$this} }));
  });
  _this.$elementsToDisplay.first().before(_this.$ul);
};

TabbedNavigator.prototype.bindEvents = function(slideShow) {
  var _this = this;
  _this.$listItems = _this.$ul.children();
  _this.$listItems.click(function() {
      var $this = $(this);
      slideShow.fadeOutIn($this);
      _this.removeCurrentClass();
      _this.addCurrentClass($this);
  });
};

TabbedNavigator.prototype.addCurrentClass = function($li) {
  $li.addClass("current");
};

TabbedNavigator.prototype.initialShowDown = function() {
  this.$listItems.first().data("divId").show();
  this.addCurrentClass(this.$listItems.first());
};

TabbedNavigator.prototype.init = function(slideShow) {
  this.appendULToDOM();
  this.bindEvents(slideShow);
  this.hideVisibleElements();
};

TabbedNavigator.prototype.removeCurrentClass = function() {
  this.$listItems.removeClass("current");
}