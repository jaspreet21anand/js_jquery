function RevealText() {
  this.paragraphHeadingElements = $("#blog h3");
};

RevealText.prototype.bindEvents = function() {
  this.paragraphHeadingElements.click(function() {
    var $heading = $(this);
    $heading.closest("li").siblings().find("p.excerpt:visible").slideUp();
    $heading.siblings("p.excerpt").slideToggle();
  });
};

$(document).ready(function() {
  var revealParagraphs = new RevealText();
  revealParagraphs.bindEvents();
});