//select all divs with class module
$('div.module').css("background-color","red");

//three methods to select third item in list with id #myList
console.log($("#myList").children()[2]);
$("#myList li:eq(2)").css("background-color","red");
$("#myList li:nth-child(3)");
//second approach is the best - because it is item specific and keeps its search limited to first level only

//select label using attribute selector
$("label[for='q']").css("background-color","red");

//figure out hidden elements
alert('hidden elements: ' + $(":hidden").length);

//find out no of image elements with alt attribute
alert('image elements: ' + $("img[alt]").length);

//select odd table rows
$("tr:odd").css("background-color","red");

//EXERCISE 2
//log alt attributes of image tag
$("img[alt]").each(function() { console.log($(this).attr("alt") ) })

//select search input box and add class
$("#q").parent().addClass("hello")

//remove class and add class to next list item
$("#myList li.current").removeClass("current").next().addClass("current")

//go at select and then traverse to submit button
$("#specials").find("select").parentsUntil("form").last().find("input[type='submit']")

//add class to first element of slideshow list and then disabled to all others following it
$("#slideshow li:eq(0)").addClass("current").nextAll().addClass("disabled")


//EXERCISE - 3
//append five elements
$("#slideshow").append($("<li><li><li><li><li>"))

//remove odd elements
$("#slideshow li:even").remove()

//add h2 and p in last div
$("div").last().append($("<h2><p>"))

//append option in select element containing value as Wednesday
$("select[name='day']").append("<option>Wednesday</option>")

//add div after last div, give it class module, add a existing image to it
$("body").children().filter("div:last").after($("<div>")).addClass("module")
$("body").children().filter("div:last").addClass("module").append($("img"))
