function formObject (form) {
  this.form = form;
  this.inputElements = this.fetchInputElements();
}

formObject.prototype.validateInputs = function() {
  for(var i = 0, len = this.inputElements.length; i < len; i++) {
    if(this.inputElements[i].value.trim() == '') {
      event.preventDefault();
      alert(this.inputElements[i].id + ' cannot be left blank!');
      return;
    }
    if(this.inputElements[i].id == 'receivenotification') {
        var reponse = confirm('please confirm about ' + this.inputElements[i].id)
        if(!reponse) {
          event.preventDefault();
          return;
        }
    }
    if(this.inputElements[i].id == 'aboutme') {
      if(this.inputElements[i].value.length < 50) {
        event.preventDefault();
        alert(this.inputElements[i].id + ' is shorter than 50 characters')
        return;
      }
    }
  }
}

formObject.prototype.fetchInputElements = function() {
  return this.form.querySelectorAll("input, textarea");
}

formObject.prototype.bindEvent = function() {
  var _this = this;
  _this.form.addEventListener("submit", function() { _this.validateInputs() });
}

window.onload = function() {
  form1 = new formObject(document.getElementById('form_1'));
  form1.bindEvent();
}

