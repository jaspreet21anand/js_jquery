function CheckBoxes(checkBoxClass, checkAllButtonId, noneButtonId) {
  this.checkBoxes = document.getElementsByClassName(checkBoxClass);
  this.checkAllButton = document.getElementById(checkAllButtonId);
  this.noneButton = document.getElementById(noneButtonId);
};

CheckBoxes.prototype.bindEvents = function() {
  var _this = this;
  _this.checkAllButton.addEventListener("click", function() { _this.toggleCheckedStatus(true) });
  _this.noneButton.addEventListener("click", function() { _this.toggleCheckedStatus(false) });
};

CheckBoxes.prototype.toggleCheckedStatus = function(value) {
  for (var i = 0; i < this.checkBoxes.length; i++) {
    this.checkBoxes[i].checked = value;
  }
};

window.onload = function() {
  var checkbox = new CheckBoxes("cb", "checkall", "none");
  checkbox.bindEvents()
};