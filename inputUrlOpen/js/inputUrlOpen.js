var regExp = {
  url : /^(?:www.)?((\w+\.)+)?((\w+\.)(\w{2,4}))(?:(\/.+)+\/?)?$/
};

function Url() {
  this.promptForUrl();
}

Url.prototype.validateUrl = function() {
  while(!this.isValidUrl()) {
    alert('invalid Url!');
    this.promptForUrl();
  }
  this.openUrl();
}

Url.prototype.isValidUrl = function() {
  return regExp["url"].test(this.url);
}

Url.prototype.openUrl = function() {
  window.open("https://" + this.url, "",
    "titlebar = no, toolbar = no, location = no, status = no, menubar = no,scrollbars = no,resizable = no,width = 400,height = 450");
};

Url.prototype.promptForUrl = function() {
  this.url = prompt('Enter url without https');
};

window.onload = function() {
  url = new Url();
  url.validateUrl();
}
