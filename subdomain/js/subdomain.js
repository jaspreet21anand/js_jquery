var regExp = {
  url : /^(?:https?:\/\/)?(?:www.)?((\w+\.)+)?((\w+\.)(\w{2,4}))(?:(\/.+)+\/?)?$/
};

function Url() {
  this.url;
};

Url.prototype.validateUrl = function() {
  return regExp["url"].test(this.url);
};

Url.prototype.extractDomain = function(event) {
  this.url = document.getElementById('url').value;
  if(this.validateUrl()) {
    this.matchAndAlertDomains();
  } else {
      event.preventDefault();
      alert("invalid url");
  }
};

Url.prototype.matchAndAlertDomains = function() {
  var match = regExp["url"].exec(this.url);
  this.alertDomains(match[3], match[1]);
};

Url.prototype.alertDomains = function(domain, subdomain) {
  if(subdomain){
    alert('subdomain: ' + subdomain + "\n" + 'domain: ' + domain);
  } else {
      alert('domain: ' + domain);
  }
};

Url.prototype.bindEvents = function() {
  var _this = this;
  document.getElementById('form').addEventListener('submit', function(event) { 
    _this.extractDomain(event);
  });
};

window.onload = function() {
  url1 = new Url();
  url1.bindEvents();
  document.getElementById('url').focus();
};
