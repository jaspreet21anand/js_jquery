function InputObject (element) {
  this.element = element;
  if(element.type) {
    this.type = element.type;
  } else {
    this.type = 'textarea';
  }
};
