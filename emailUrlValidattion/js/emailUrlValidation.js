function formObject (form) {
  this.form = form;
  this.inputObjects = [];
};

formObject.prototype.validateInputs = function(event) {
  for(var i = 0, len = this.inputObjects.length; i < len; i++) {
    if(this.inputObjects[i].type == 'text') {
      if(!this.inputObjects[i].element.value.trim()) {
        event.preventDefault();
        alert(this.inputObjects[i].element.id + ' cannot be left blank!')
        return;
      }
      if(this.inputObjects[i].element.id == 'email') {
        var regExp = /^([a-zA-Z0-9]?)+([a-zA-Z0-9][._]?[a-zA-Z0-9]+)+[a-zA-Z0-9]?@([a-zA-Z0-9]+\.)+([a-zA-Z]{2,4})$/;
        if(!regExp.test(this.inputObjects[i].element.value)) {
          event.preventDefault();
          alert(this.inputObjects[i].element.id + ' is not valid ' + this.inputObjects[i].element.id + '!')
          return;
        }
      } else if(this.inputObjects[i].element.id == 'url') {
          var regExp = /^(?:https?:\/\/)?(?:www.)?((\w+[.-])+)?\w+\.\w{2,4}((\/([\S]?[\w]+)+([\S]?)+)?)+\/?$/;
          if(!regExp.test(this.inputObjects[i].element.value)) {
            event.preventDefault();
            alert(this.inputObjects[i].element.id + ' is not valid ' + this.inputObjects[i].element.id + '!');
            return;
          }
      }
    } else if(this.inputObjects[i].type == 'checkbox') {
        if(!this.inputObjects[i].element.checked) {
          var response = confirm(this.inputObjects[i].element.id + ' is not checked!');
          if(!response) {
            event.preventDefault();
          }
        }
    } else if(this.inputObjects[i].type == 'textarea') {
        if(this.inputObjects[i].element.value == '' || this.inputObjects[i].element.value.length < 50) {
          event.preventDefault();
          alert(this.inputObjects[i].element.id + ' is shorter than 50 characters');
          return;
        }
    }
  }
};

formObject.prototype.addInputObjects = function() {
  var temp = this.form.getElementsByClassName('inputfield');
  for(var i = 0, len = temp.length; i < len; i++) {
    this.inputObjects[i] = new InputObject(temp[i]);
  }
};

formObject.prototype.bindEvent = function() {
  var _this = this;
  _this.form.addEventListener("submit", function(event) { _this.validateInputs.call(_this, event) });
};

window.onload = function() {
  form1 = new formObject(document.getElementById('form_1'));
  form1.addInputObjects();
  form1.bindEvent();
  document.getElementById("loginId").focus();
};

