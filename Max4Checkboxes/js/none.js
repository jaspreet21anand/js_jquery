//none functionality
function None(id) {
  this.element = document.getElementById(id);
}

None.prototype.bindEvents = function(listObject) {
  var _this = this;
  this.element.addEventListener("click", function() { _this.unCheckAll(listObject); });
}

None.prototype.unCheckAll = function(listObject) {
  for(var i = 0, len = listObject.selectedItems.length; i < len; i++) {
    document.getElementById(listObject.selectedItems[i]).checked = false;
  }
  listObject.selectedItems = [];
}
