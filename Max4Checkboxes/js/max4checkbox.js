
function List(listId, maxSelectionAllowed) {
  this.listElement = document.getElementById(listId);
  this.listItems = document.getElementsByClassName("days");
  this.selectedItems = [];
  this.maxSelectionAllowed = maxSelectionAllowed;
}

List.prototype.bindEvents = function(noneObject) {
  var _this = this;
  for(var i = 0, len = _this.listItems.length; i < len; i++) {
    _this.listItems[i].addEventListener("click", function() {
      noneObject.element.checked = false;
      _this.updateSelected(this);
    });
  }
}

List.prototype.updateSelected = function(changedItem) {
  if(changedItem.checked){
    this.selectedItems.push(changedItem.id);
  } else {
    this.selectedItems.splice(this.selectedItems.indexOf(changedItem.id),1)
  }
  this.verifyMaxAllowedSelection(changedItem);
}

List.prototype.verifyMaxAllowedSelection = function(changedItem) {
  if(this.selectedItems.length > this.maxSelectionAllowed) {
    changedItem.checked = false;
    this.selectedItems.splice(this.selectedItems.indexOf(changedItem.id),1);
    this.displayMessage();
  }
};

List.prototype.displayMessage = function() {
  alert('You can not select more than ' + this.maxSelectionAllowed + ' options and you have selected ' + this.selectedItems.join(', '))
}

window.onload = function() {

  //checkbox list object
  checkBoxList = new List('list', 3);
  checkNoneObject = new None('none');

  checkNoneObject.bindEvents(checkBoxList);
  checkBoxList.bindEvents(checkNoneObject);
  document.getElementById("Monday").focus();
}