function Quiz() {
  this.score = 0;
  this.questionNumber = 1;
  this.numberOfQuestions = 20;
  this.wronglyAnswerd = [];
  this.$nextButton = $("#nextQuestion");
  this.$quizDiv = $("#quizDiv");
  this.questions = this.createQuestions();
};

Quiz.prototype.createQuestions = function() {
  var quesArray = [];
    for(var i = 0; i < this.numberOfQuestions; i++) {
      quesArray[i] = new Question();
    }
  return quesArray;
};

Quiz.prototype.askNextQuestion = function() {
  commonConstantsForQuiz.$questionLabel.text(this.questionNumber + ": " + this.questions[this.questionNumber - 1].question);
};

Quiz.prototype.checkAnswer = function() {
  return commonConstantsForQuiz.$answerBox.val() == this.questions[this.questionNumber - 1].answer;
};

Quiz.prototype.displayScore = function() {
  commonConstantsForQuiz.$scoreLabel.text("score: " + this.score + "/" + this.numberOfQuestions);
};

Quiz.prototype.finishQuiz = function() {
  this.$quizDiv.empty();
  var div = $("<div />").append($("<text /><br>").text("Score: " + this.score));
  for(var i = 0, len = this.wronglyAnswerd.length; i < len; i++) {
    var quesAnswerElement = ("<text>" + this.questions[this.wronglyAnswerd[i] - 1].question + "=" + this.questions[this.wronglyAnswerd[i] - 1].answer + "</text><br>");
    div.append($(quesAnswerElement));
  }
  this.$quizDiv.append(div);
};

Quiz.prototype.updateScore = function() {
  if(this.checkAnswer()) {
    this.score++;
    this.displayScore();
  } else {
    this.wronglyAnswerd.push(this.questionNumber);
  }
  commonConstantsForQuiz.$answerBox.val('').focus();
};

Quiz.prototype.bindEvents = function() {
  var _this = this;
  this.$nextButton.on("click", function() {
    _this.updateScore();
    if(_this.questionNumber++ < _this.numberOfQuestions) {
      _this.askNextQuestion();
    } else {
        _this.finishQuiz();
    }
  });
};

$(document).ready(function() {
  var quiz = new Quiz();
  quiz.askNextQuestion();
  quiz.bindEvents();
});