var commonConstantsForQuiz = {
  $answerBox : $("#answer"),
  $questionLabel : $("#question"),
  $scoreLabel : $("#score"),
  minimumNumber : 1,
  maxNumber : 20
};

function Question () {
  this.firstNumber = this.generateNumber();
  this.secondNumber = this.generateNumber();
  this.operator = this.generateOperator();
  this.question = this.firstNumber + this.operator + this.secondNumber;
  this.answer = parseInt(eval(this.firstNumber + this.operator + this.secondNumber));
};

Question.prototype.generateNumber = function() {
  return parseInt(Math.random() * commonConstantsForQuiz.maxNumber + commonConstantsForQuiz.minimumNumber);
};

Question.prototype.generateOperator = function() {
  return ["*", "/", "+", "-"][parseInt(Math.random() * 4)];
};
