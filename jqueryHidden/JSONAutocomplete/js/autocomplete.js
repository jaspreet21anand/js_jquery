function AutoComplete(textBox, suggestionList) {
  this.users           = null;
  this.$textBox        = textBox;
  this.$suggestionList = suggestionList;
}

AutoComplete.prototype.sendAJAXAndStoreUsers = function() {
  var _this = this;
  $.get("js/users.json", function(userJSON) {
    _this.users = userJSON;
  }, "json");
};

AutoComplete.prototype.populateSuggestions = function() {
  var value = this.$textBox.val(),
      _this = this;
  if(value) {  
    var regexp = new RegExp(value, "i");
    this.$suggestionList.empty();
    this.users.forEach(function(user) {
      if(regexp.test(user["name"])) {
        _this.$suggestionList.append($("<li />", { 'text': user["name"] }));
      }
    });
  } else {
    this.$suggestionList.hide();
  }
};

AutoComplete.prototype.displaySuggestions = function() {
  this.$suggestionList.show();
  this.populateSuggestions();
};

AutoComplete.prototype.bindEvents = function() {
  var _this = this;
  this.$textBox.on("keyup paste", function(event) {
    _this.displaySuggestions();
  });
  this.$suggestionList.on("mouseenter mouseleave", "li", function() {
    $(this).toggleClass("highlight");
  })
  this.$suggestionList.on("click", "li", function() {
    _this.$textBox.val($(this).text());
    _this.$suggestionList.hide();
   });
};

$(function() {
  var nameHint = new AutoComplete($("#name"), $("#suggestionList"));
  nameHint.sendAJAXAndStoreUsers();
  nameHint.bindEvents();
});