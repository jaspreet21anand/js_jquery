function ContainerElement(container, addItemButton) {
  this.$container = container;
  this.$addItemButton = addItemButton;
  this.elementCount = 0;
};

ContainerElement.prototype.addElementInContainer = function() {
  this.$container.append($("<div />", { 'class':'inner', 'text':++this.elementCount }));
};

ContainerElement.prototype.bindEvents = function() {
  this.clickHandlerForDivsButLast();
  this.lastDivClickHandler();
  this.addItemButtonClickHandler();
};

ContainerElement.prototype.clickHandlerForDivsButLast = function() {  
  this.$container.on("click", "div.inner:not(:last)", function() {
    $(this).toggleClass("highlight");
  });
};

ContainerElement.prototype.lastDivClickHandler = function() {
  var _this = this;
  this.$container.on("click", "div.inner:last", function() {
    this.remove();
    --_this.elementCount;
  });
};

ContainerElement.prototype.addItemButtonClickHandler = function() {
  var _this = this;
  _this.$addItemButton.click(function() {
    _this.addElementInContainer();
  });
};

$(function() {
  (new ContainerElement($("#container"), $("#addItem"))).bindEvents();
});