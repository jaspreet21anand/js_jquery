var categoryItemObjectsArray = {
  "bread": [
  {
    "itemName" : "wheat bread",
    "itemPrice" : 5 
  },
  {
    "itemName" : "multi grain",
    "itemPrice" : 15 
  },
  {
    "itemName" : "italian",
    "itemPrice" : 10 
  },
  ],
  "filling": [
  {
    "itemName": "veg",
    "itemPrice": 6
  },
  {
    "itemName": "non-veg",
    "itemPrice": 7
  },
  {
    "itemName": "egg",
    "itemPrice": 18
  }
  ],
  "sauce": [
  {
    "itemName":"tomato",
    "itemPrice": 25
  },
  {
    "itemName":"mustard",
    "itemPrice": 17
  }
  ],

   "sauce1": [
  {
    "itemName":"tomato",
    "itemPrice": 25
  },
  {
    "itemName":"mustard",
    "itemPrice": 17
  }
  ]

}