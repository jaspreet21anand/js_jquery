function PersonalOrder() {
  this.customerName = '';
  this.itemList = [];
  this.orderElement = "<div>";
  this.totalAmount = 0;
  this.orderJqueryElement = null;
}

PersonalOrder.prototype.addItem = function(item) {
  this.totalAmount += item.itemPrice;
  this.itemList.push(item);
}

PersonalOrder.prototype.removeItem = function(item) {
  this.totalAmount -= item.itemPrice;
  this.itemList.splice($.inArray(item, this.itemList), 1);
}

PersonalOrder.prototype.removeItemOfCategory = function(category) {
  for(var i = 0; i < this.itemList.length; i++) {
    if(this.itemList[i].category == category) {
      this.removeItem(this.itemList[i]);
    }
  }
}

PersonalOrder.prototype.addNameToOrder = function() {
  while(!/^\w+(\s)?(\w+)?/.test(this.customerName)) {
    this.customerName = prompt('Please enter your name');
  }
  this.orderJqueryElement.prepend($("<h4 />", { 'html':this.customerName }));
}

PersonalOrder.prototype.createJqueryElementObject = function() {
  var order = $(this.orderElement, { 'html': generateHTML.call(this), 'class':'order', 'data':{ 'itemObject':this }});
  
  function generateHTML() {
    var tempElement = '';
    for(var i = 0, len = this.itemList.length; i < len; i++) {
      tempElement = tempElement + this.itemList[i].itemName + "------" + this.itemList[i].itemPrice + "<br>"; 
    }
    return (tempElement + "total Amount: " + this.totalAmount);
  }

  this.orderJqueryElement = order.data('itemJqueryObject', order);
  return this.orderJqueryElement;
}
