function Item(itemObjectFromJson, category) {
  this.itemName = itemObjectFromJson.itemName;
  this.itemPrice = itemObjectFromJson.itemPrice;
  this.itemElement = "<div>";
  this.category = category;
}

Item.prototype.createJqueryElementObject = function() {
  this.itemJqueryObject = $(this.itemElement, { 'html':this.itemName + "<br>" + this.itemPrice, 'class':'item ' + this.category, 'data':{ 'itemObject':this } });
  return this.itemJqueryObject.data('itemJqueryObject', this.itemJqueryObject)
}
