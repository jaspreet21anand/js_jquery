function Kiosk() {
  this.orderOptionsGrid = {
    'div': $("#categoryList"),
    'categoryObjectsArray': []
  };
  this.personalOrder = null;
  this.totalOrder = new TotalOrder();
  this.$totalAmountElement = $("#totalAmount");
  this.$personalOrderDisplayElement = $("#personalOrder")
}

Kiosk.prototype.createAndAddCategoryToDOM = function(categoryName) {
  var category = new Category(categoryName)
  this.orderOptionsGrid.categoryObjectsArray.push(category)
  this.orderOptionsGrid.div.append(category.createJqueryElementObject());
}

Kiosk.prototype.createNewPersonalOrderAndAppendToDOM = function() {
  this.personalOrder = new PersonalOrder();
  this.appendPersonalOrderToDOM();
}

Kiosk.prototype.appendPersonalOrderToDOM = function() {
  var placeFinder = this.$personalOrderDisplayElement.find("h3")
  placeFinder.next('div').remove();
  placeFinder.after(this.personalOrder.createJqueryElementObject());
}

Kiosk.prototype.updateTotalAmountElement = function() {
  this.$totalAmountElement.find("text").text(this.totalOrder.totalAmount);
}

Kiosk.prototype.bindEvents = function() {
  this.bindEventsOnItems();
  this.bindEventOnPlaceOrderButton();
  this.bindEventsOnOrder();
}

Kiosk.prototype.bindEventsOnItems = function() {
  this.orderOptionsGrid.div.on("mouseenter mouseleave", ".item", function() {
    $(this).toggleClass("highlight");
  });

  var _this = this;
  this.orderOptionsGrid.div.on("click", ".item", function(event) {
    var category = $(event.target).parent().attr("id")
    var item = $(this).data("itemObject")

    //toggle class
    $(".item.selected." + category).not(this).removeClass("selected");
    $(this).toggleClass("selected");

    //add or remove items from order
    var itemIndex = $.inArray(item, _this.personalOrder.itemList)
    if(itemIndex == -1) {
      _this.personalOrder.removeItemOfCategory(category);
      _this.personalOrder.addItem(item);
    } else {
        _this.personalOrder.removeItem(item);
    }

    //updating changed order on html page
    _this.appendPersonalOrderToDOM()
  });
}

Kiosk.prototype.bindEventOnPlaceOrderButton = function() {
  var _this = this;
  $("#placeOrder").click(function() {
    _this.personalOrder.addNameToOrder();
    _this.totalOrder.addOrderAndAppendToDOM($(this).siblings('div'));
    _this.updateTotalAmountElement();
    _this.createNewPersonalOrderAndAppendToDOM();
    $(".selected").removeClass("selected");
  });
}

Kiosk.prototype.bindEventsOnOrder = function() {
  $(".orderDiv").on("mouseenter mouseleave", ".order", function() {
    var $order = $(this);
    $order.toggleClass("orderHighlight");
    var itemArray = $order.data("itemObject").itemList;
    for(var i = 0, len = itemArray.length; i < len; i++) {
      itemArray[i].itemJqueryObject.toggleClass("orderHighlight")
    }
  });
}

Kiosk.prototype.init = function() {
  for(var category in categoryItemObjectsArray) {
   this.createAndAddCategoryToDOM(category); 
  }
  this.createNewPersonalOrderAndAppendToDOM();
  this.updateTotalAmountElement();
  this.bindEvents();
}

$(document).ready(function() {
  var foodKiosk = new Kiosk();
  foodKiosk.init();
});