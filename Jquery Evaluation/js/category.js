function Category(name) {
  this.categoryName = name;
  this.categoryItems = [];
  this.categoryElement = null;
  this.initialize();
}

Category.prototype.initialize = function() {
  var itemsArray = categoryItemObjectsArray[this.categoryName]
  for(var i = 0, len = itemsArray.length; i < len; i++) {
    this.categoryItems.push(new Item(itemsArray[i], this.categoryName));
  }
}

Category.prototype.createJqueryElementObject = function() {
  this.categoryElement = $("<div>", { 'class':'category', 'id': this.categoryName });
  for(var i = 0, len = this.categoryItems.length; i < len; i++) {
    this.categoryElement.append(this.categoryItems[i].createJqueryElementObject());
  }
  this.categoryElement.prepend($("<h4 />", { 'text':this.categoryName }))
  return this.categoryElement;
}
