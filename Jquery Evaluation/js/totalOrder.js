function TotalOrder() {
  this.orderArray = [];
  this.totalAmount = 0;
  this.totalOrderContainer = $("#totalOrder");
}

TotalOrder.prototype.addOrderAndAppendToDOM = function(orderJqueryElement) {
  var orderObject = orderJqueryElement.data("itemObject")
  this.orderArray.push(orderObject);
  this.totalAmount += orderObject.totalAmount;
  this.appendOrderToDOM(orderJqueryElement)
}

TotalOrder.prototype.appendOrderToDOM = function(orderJqueryElement) {
  this.totalOrderContainer.prepend(orderJqueryElement);
}