function ListPair(arguments) {
  this.list_1 = arguments.list1;
  this.list_2 = arguments.list2;
  this.addButton = arguments.addButton;
  this.removeButton = arguments.removeButton;
}

//for transfer=true-> list1=>list2 and vice versa
ListPair.prototype.transferSelectedOptions = function(transfer) {
  if(transfer) {
    var list1 = this.list_1;
    var list2 = this.list_2;
  } else {
      var list1 = this.list_2;
      var list2 = this.list_1;
    }
  var selectedItems = list1.selectedOptions;
  while(selectedItems[0]) {
    list2.appendChild(selectedItems[0]);
    list2.selectedIndex = -1;
  }
}

ListPair.prototype.bindEvents = function() {
  var _this = this;
  _this.addButton.addEventListener("click", function() {
    _this.transferSelectedOptions(true)
  });
  _this.removeButton.addEventListener("click", function() {
    _this.transferSelectedOptions(false);
  });
  _this.list_1.addEventListener("focus", function() { _this.list_2.selectedIndex = -1 });
  _this.list_2.addEventListener("focus", function() { _this.list_1.selectedIndex = -1 });
}

window.onload = function() {
  var listAndButtons = {
    list1 : document.getElementById("list1"),
    list2 : document.getElementById("list2"),
    addButton : document.getElementById("add"),
    removeButton : document.getElementById("remove")
  }

  var listPair = new ListPair(listAndButtons);
  listPair.bindEvents();
}
