//class for list
function List(listOpeningCheckBox) {
  this.listOpeningCheckBox = listOpeningCheckBox;
};

List.prototype.bindEvents = function() {
  var _this = this;
  _this.listOpeningCheckBox.addEventListener("click", function() { _this.showOrHideList() });
}

List.prototype.showOrHideList = function() {
  if(this.listOpeningCheckBox.checked) {
    this.showList();
  } else {
    this.hideList();
  }
}

List.prototype.showList = function() {
  this.ul = this.listOpeningCheckBox.parentElement.parentElement;
  this.ul.querySelector("ul").style.display = "block";
  this.ul.scrollIntoView(true);
  this.changeListItemSelection(true);
}

List.prototype.hideList = function() {
  this.changeListItemSelection(false);
  this.ul.querySelector("ul").style.display = "none";
}

//clear selection in a list when unchecked and vice versa
List.prototype.changeListItemSelection = function(value) {
  var checkBoxes = this.ul.querySelectorAll("ul input");
  for(var i = 0, len = checkBoxes.length; i < len; i++) {
    checkBoxes[i].checked = value;
  }
}

window.onload = function() {
  var lists = document.getElementsByClassName("listOpener");
  var listObjects = [];
  for (var i = 0, len = lists.length; i < len; i++) {
    listObjects[i] = new List(lists[i]);
    listObjects[i].bindEvents();
  };
}