function User(name, age) {
  this.name = name;
  this.age = age;
}

//this fucntion can be reused for sorting
User.prototype.compareAge = function(user) {
  if (this.age < user.age) {
    return -1;
  }
  else if (this.age > user.age) {
    return 1;
  }
  else {
    return 0;
  }
};

User.prototype.print_age_message = function(user) {
  if (this.compareAge(user) < 0) {
    alert(user.name + ' is older than ' + this.name);
  }
  else if (this.compareAge(user) > 0) {
    alert(this.name + ' is older than ' + user.name);
  }
  else {
    alert(this.name + ' is as old as ' + user.name);
  }
};
function run() {
  var user1 = new User(document.getElementById('name1').value, parseInt(document.getElementById('age1').value)),
      user2 = new User(document.getElementById('name2').value, parseInt(document.getElementById('age2').value));
  user1.print_age_message(user2);
}
