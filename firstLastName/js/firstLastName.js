function User() {
  this.firstName;
  this.lastName;
}

User.prototype.getName = function() {
  this.firstName = this.promptInputAndValidate("Enter first Name");
  this.lastName = this.promptInputAndValidate("Enter last Name");
};

User.prototype.promptInputAndValidate = function(message) {
  var name = '';
  while(name == null || name.trim() == ''){
    name = prompt(message);
  }
  return name;
}

User.prototype.writeNameOnPage = function(element) { 
  alert('Welcome ' + this.firstName + ' ' + this.lastName);
  element.innerHTML = this.firstName + ' ' + this.lastName;
}

window.onload = function() {
  var user = new User();
  user.getName();
  user.writeNameOnPage(document.getElementById('toBeWritten'));
}